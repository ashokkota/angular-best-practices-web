## Techgrit CSS Style Guide

#### **Global Styling**
>Why?  We can keep things simply by having one global `styles.scss` SCSS file, and we can also use styling on a component-by-component basis.
>
>It is best to write styles in global as possible avoiding component styling. Especially when working on teams with multiple developers. 
>
>Having global styles keeps everyone using the same structure and system, and encourages discussions about patterns and use cases.


## CSS/LESS/SCSS format extension (VS Code)
> Formate: CSS, LESS and SCSS Formatter (Install through VS Code extensions. Search for formate.) 

```bash 
How to use
On Windows:
	CTRL + Shift + P -> Format Document
	CTRL + ALT + F

On Mac:
	CMD + Shift + P -> Format Document
	CMD + ALT + F
```

## Folder Structure

```bash
├───styles
│   ├───themes
	│   └───theme-1.scss
	│   └───theme-2.scss
│   ├───typography.scss
│   ├───variable.scss
├───style.scss
```
## CSS Naming Conventions
```bash
// bad 
.redBox {  border: 1px solid red;}

// good
.red-box {   border: 1px solid red;}
```

Add Project name to css class (recommended)
```bash
// good
.techgrit-red-box{  border: 1px solid red;}

// or add some short name
.tg-red-box {   border: 1px solid red;}

.tg-nav-bg { background-c0lor: red; } 
```

## Write More CSS Comments
```bash
Use below comment for section divider 
/* ==========================================
 Section comment block
  =========================================== */


/* ==========================================
 Header
  =========================================== */
  
/* ==========================================
 Footer
  =========================================== */

/* ==========================================
 Nav-bar
  =========================================== */
```

```bash
Use below comment for sub-section divider 
/* Sub-section comment ============================================== */
```

```bash
Basic Comments

/* Basic comment */
```

## Properties Order
```bash
First  -  Positioning 
Second - Display & Box Model
Other

.selector {
    /* Positioning */
    position: absolute;
    z-index: 10;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;

    /* Display & Box Model */
    display: inline-block;
    overflow: hidden;
    box-sizing: border-box;
    width: 100px;
    height: 100px;
    padding: 10px;
    border: 10px solid #333;
    margin: 10px;

    /* Other */
    background: #000;
    color: #fff;
    font-family: sans-serif;
    font-size: 16px;
    text-align: right;
}
```

## CSS Selector Rules
```bash

 The * selector should be avoided for performance reasons.
	// bad
	* {
		font-family: '';
	}
	
	// good
	html {
		font-family: ''
	}
```

```bash
Element selectors are useful for creating base styles.
	 
	 // good
	 p { 
		padding: 10%;
	 }
	 
	 h1, 
	 h2,
	 h3 {
		padding: 5%;
	 }
```

```bash 
Classes are mainly used for all the scenarios
   .tg-nav-bar-bg {
	   background-color: red;
   }
 ```

 ```bash
IDs should be reserved for “unique” styles. For example, if we want to override any of the class or selector styles then we can use Id. [instead for using !important we can use Ids]
 Example:
	
	// bad
	<p class='color-red'> Hello, Welcome </p>
	
	 p {
		 color: red;
	 } 
	 
     //to override p tag color
     .color-red {
	     color: white !important;
     }

	// good
	<p class='color-red' id="color-red"> Hello, Welcome </p>
	
	 p {
		 color: red;
	 } 
	 
     //to override p tag color
     #color-red {
	     color: white;
     }
```
